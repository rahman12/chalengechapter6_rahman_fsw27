'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.user_game_history.hasOne(models.user_game);

      models.user_game_history.hasMany(models.user_game,{as: "history"})
    }
    
  }
  user_game_history.init({
    title: DataTypes.STRING,
    game_loaded: DataTypes.STRING,
    waktu: DataTypes.STRING,
    level: DataTypes.TEXT,
    approved: DataTypes.BOOLEAN,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_game_history',
  });
  return user_game_history;
};