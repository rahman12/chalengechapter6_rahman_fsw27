const express = require ("express");
//const { User } = require("sequelize/types/query-types");
const app = express ();
const { Article,user_game_biodata,user_game_history } = require('./models');
const user_game = require("./models/user_game");

app.use(express.json());
app.use(express.urlencoded({extended: false}));

const validation = (req, res, next) => {
    const { id } = req.body;
    const isFind = data.find((row) => row.id == id);
    if( isFind){
        res.status (400).json({message : "id not available"});
    } else {
        next();
    }
};

user_game_biodata.create ({
    user_name: user_name,
    password : password,
    email : email,
})
    .then(user_game_biodata =>{
        res.json({message : "create Success", data: user_game_biodata})
    })
    

app.post('/articles', (req, res ,) =>{
    const {title, body, approved} = req.body;

    Article.create({
        title: title,
        body: body,
        approved: approved,
        })
        .then(article => {
        res.json({message : "create Success", data: article})
        })
})

app.get ("articles", (req, res) =>{
    Article.findAll({include:[{model: User, as:"user"}]}).then ((article) =>{
        res.json({ message : "fetch All Success", data: article});
    });
});

app.get ("articles", (req, res) =>{
    const { id } = req.params;

    Article.findOne({where : {id : id} }).then ((article) =>{
        res.json({ message : "fetch All Success", data: article});
    });
});

app.put("/articles/:id",(req, res)=> {
    const { id } = req.params
    Article.update({approved: false}, { where: { id : id }})
        .then(() => {
        res.json({ message : "Update Success"});
        })
        .catch(err => {
        res.json({ message : "Gagal mengupdate artikel!"});
        });
});

app.get("/user_game",(req, res)=>{
    user_game.findAll({include:["history","user_game_biodata"]}).then((user_game)=>{
        res.json({message: "fetch all succes", data: user_game});
    });
});

app.get("/views/articles/create", (req, res)=>{
    res.render("article/form");
});

app.use((req, res, next)=>{
    res.status (400).json ({message: "sorry cant find that" });
});

app.listen(5000,() => console.log(`Listening at http://localhost:${5000}`))